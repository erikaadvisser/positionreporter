package org.n1.pos.model;


import java.util.HashMap;
import java.util.Map;

public class Beacon {
    private Map<Long, Integer> rssiByTimestamp = new HashMap<>();
    private long tickStart;
    private int lastAverage = 0;
    private int min = 1000;
    private int max = -1000;
    private int avgTotal;
    private int avgCount;
    private int count = 0;



    public Beacon(int rssi) {
        this.tickStart = System.currentTimeMillis();
        addRssi(rssi);
    }

    public void addRssi(int rssi) {
        long now = System.currentTimeMillis();
        int ss = toSS(rssi);
        rssiByTimestamp.put(now, ss);
        count++;
    }

    private int toSS(int rssi) {
        return 50 + (rssi / 2);
    }


    public String description() {
        long now = System.currentTimeMillis();
        long passed = now - tickStart;
        if (passed > 2000) {
            lastAverage = calcAverage();
            tickStart = now;
        }
        if (avgCount == 0) {
            return "measuring...";
        }

        int longAverage = (avgTotal / avgCount);
        return String.format("avg: %d [%d] %d - %d (%d)", lastAverage, longAverage, min, max, count);
    }

    private int calcAverage() {
        if (rssiByTimestamp.size() == 0) {
            return lastAverage;
        }
        long total = 0;
        for (long rssi : rssiByTimestamp.values()) {
            total += rssi;
        }
        int avg = (int) (total / rssiByTimestamp.size());
        rssiByTimestamp = new HashMap<>();

        max = Math.max(avg, max);
        min = Math.min(avg, min);
        avgTotal += avg;
        avgCount++;

        return avg;
    }

}
