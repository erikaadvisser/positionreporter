package org.n1.pos.service;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class RestService {

    private final VolleyRequestQueue queue;

    public RestService(Context context) {
        queue = VolleyRequestQueue.getInstance(context);
    }

    public void getLocations(final Response.Listener<List<String>> listener) {
        Log.d("rest", "Location request start");
        String url = "http://leng:8080/api/location/";
        JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {
                Log.d("rest", "Location request received");
                List<String> locations = new LinkedList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    String location = null;
                    try {
                        location = jsonArray.getString(i);
                    } catch (JSONException e) {
                        location = "Exception: " + e.getMessage();
                    }
                    locations.add(location);
                }
                listener.onResponse(locations);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("rest", "REST ERROR");
                List<String> response = Arrays.asList("Failure communicating with server", volleyError.getMessage());
                listener.onResponse(response);
            }
        });

        queue.add(arrayRequest);
    }


}
