package org.n1.pos.ui.location_chooser;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.android.bluetoothlegatt.R;

import org.n1.pos.ui.util.ListViewHolder;

import java.util.Collections;
import java.util.List;

public class LocationListAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private List<String> locations = Collections.singletonList("Initializing");

    public LocationListAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return locations.size();
    }

    @Override
    public Object getItem(int position) {
        return locations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ListViewHolder viewHolder;
        // General ListView optimization code.
        if (view == null) {
            view = inflater.inflate(R.layout.listitem_device, null);
            viewHolder = new ListViewHolder();
            viewHolder.description = (TextView) view.findViewById(R.id.description);
            viewHolder.title = (TextView) view.findViewById(R.id.title);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ListViewHolder) view.getTag();
        }

        viewHolder.title.setText(locations.get(position));
        viewHolder.description.setText("");

        return view;
    }

    public void locations(List<String> locations) {
        this.locations = locations;
    }
}
