package org.n1.pos.ui.measure;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.android.bluetoothlegatt.R;


public class MeasureActivity extends Activity {

    public static final String EXTRA_LOCATION = "location";
    private static final int PERMISSION_REQUEST = 1;

    private BeaconListAdapter beaconListAdapter;
    private BluetoothAdapter bluetoothAdapter;

    private boolean scanning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_measure);

        String location = getIntent().getStringExtra(EXTRA_LOCATION);
        getActionBar().setTitle(location);

        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN}, PERMISSION_REQUEST);

    }

    @Override
    protected void onResume() {
        super.onResume();

        beaconListAdapter = new BeaconListAdapter(getLayoutInflater());
        ((ListView) findViewById(R.id.beaconList)).setAdapter(beaconListAdapter);
        scanLeDevice(true);
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            scanning = true;
            bluetoothAdapter.startLeScan(bleScanCallback);
        } else {
            scanning = false;
            bluetoothAdapter.stopLeScan(bleScanCallback);
        }
    }

    //
//
    // Device scan callback.
    private BluetoothAdapter.LeScanCallback bleScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("ble", device.getName());
                            beaconListAdapter.receiveDeviceData(device, rssi);
                            beaconListAdapter.notifyDataSetChanged();
                        }
                    });
                }
            };


}
