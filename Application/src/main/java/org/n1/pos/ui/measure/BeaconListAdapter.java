package org.n1.pos.ui.measure;

import android.bluetooth.BluetoothDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.android.bluetoothlegatt.R;

import org.n1.pos.model.Beacon;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class BeaconListAdapter extends BaseAdapter {
    private List<BluetoothDevice> devices = new LinkedList<>();
    private Map<BluetoothDevice, Beacon> beacons = new HashMap<>();
    private final LayoutInflater inflater;

    public BeaconListAdapter(LayoutInflater inflater) {
        super();
        this.inflater = inflater;
    }

    public void receiveDeviceData(BluetoothDevice device, int rssi) {
        if (!beacons.containsKey(device)) {
            Beacon beacon = new Beacon(rssi);
            beacons.put(device, beacon);
            devices.add(device);
        }
        beacons.get(device).addRssi(rssi);
    }

    public void clear() {
        beacons.clear();
        devices.clear();
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public Object getItem(int i) {
        return devices.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int number, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        // General ListView optimization code.
        if (view == null) {
            view = inflater.inflate(R.layout.listitem_device, null);
            viewHolder = new ViewHolder();
            viewHolder.deviceAddress = (TextView) view.findViewById(R.id.description);
            viewHolder.deviceName = (TextView) view.findViewById(R.id.title);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        BluetoothDevice device = devices.get(number);
        final String deviceName = device.getName();
        if (deviceName != null && deviceName.length() > 0)
            viewHolder.deviceName.setText(deviceName);
        else
            viewHolder.deviceName.setText(R.string.unknown_device);

        String description = beacons.get(device).description();
        viewHolder.deviceAddress.setText(description);

        return view;
    }

    private static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
    }
}
